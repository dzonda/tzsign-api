
generate_rpc_client:
	swagger generate client -t services/rpc_client -f ./services/rpc_client/client.yml -A tezosrpc

init:
	docker compose up -d api-db
	docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' api-db		

start:
	docker compose up api-db api-server

stop:
	docker compose down

clean:
	docker compose down --volumes

generate_keys:
	@cd keys && go build
	@./keys/tezosign-keys
