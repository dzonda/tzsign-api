module tezosign

go 1.15

require (
	blockwatch.cc/tzindex v0.0.0-20210312080332-e4415acf16a1
	github.com/anchorageoss/tezosprotocol/v3 v3.0.0
	github.com/btcsuite/btcd v0.22.1
	github.com/echa/log v1.1.1 // indirect
	github.com/go-openapi/analysis v0.21.3 // indirect
	github.com/go-openapi/errors v0.20.2
	github.com/go-openapi/runtime v0.24.1
	github.com/go-openapi/spec v0.20.6 // indirect
	github.com/go-openapi/strfmt v0.21.2
	github.com/go-openapi/swag v0.21.1
	github.com/golang-jwt/jwt/v4 v4.4.1
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/gorilla/securecookie v1.1.1
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/petermattis/goid v0.0.0-20220526132513-07eaf5d0b9f4 // indirect
	github.com/pkg/errors v0.9.1
	github.com/roylee0704/gron v0.0.0-20160621042432-e78485adab46
	github.com/rs/cors v1.8.2
	github.com/sasha-s/go-deadlock v0.3.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/tendermint/tendermint v0.35.9
	github.com/urfave/negroni v1.0.0
	github.com/wedancedalot/decimal v0.0.0-20180831135040-d09bcccc62ea
	go.mongodb.org/mongo-driver v1.9.1 // indirect
	go.uber.org/zap v1.21.0
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
	golang.org/x/xerrors v0.0.0-20220517211312-f3a8303e98df
	gorm.io/driver/postgres v1.3.7
	gorm.io/gorm v1.23.5
)
