package models

type Network string

const (
	NetworkMain     Network = "main"
	NetworkDelphi   Network = "delphi"
	NetworkEdo      Network = "edo"
	NetworkFlorence Network = "florence"
	NetworkIthaca 	Network = "ithaca"
	NetworkJakarta 	Network = "jakarta"
	NetworkGhost 	Network = "ghost"
)
