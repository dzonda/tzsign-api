# TzSign Wallet

TzSign is the first Tezos multisig web wallet developed by Attic Lab and based on the TQ’s multisig contract. TzSign allows you to create and send transactions, delegations and will also provide the vesting functionality.

The wallet supports Beacon authentication and currently runs on the Delphi testnet.

## Project overview

Programming language: `Go v1.15.2`

DB: `Postgres 12`

Tezos Node: `mainnet.api.tez.ie`

## How to run

### Prerequisites

#### TzKT

You must have a TzKT database. Follow the instructions [here](https://github.com/baking-bad/tzkt).

#### Database

You need to set up a database with a predefined schema.

Run the command `make init` to set up one with docker. If you also run the API server with docker, you can fill in the IP displayed after this command in your configuration file.

#### Keys

You need to generate keys to secure the application.

Run the command `make generate_keys` to generate new ones.

#### Configuration

You need to set up your configuration.

Place a file named `config.json` in a `.secrets` folder, at the root of the repository.

An example of the configuration file is `config_default.json` or `config_example.json`.

##### Networks

You can put several objects to instantiate several different networks.

Valid network names are `main`, `ghost` and `jakarta`.

For the Auth part, please refer to [this section](#keys) to generate your keys.
